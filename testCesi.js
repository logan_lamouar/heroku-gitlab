var webdriver = require('selenium-webdriver'), driver;
 
let driver = new webdriver.Builder()
    .forBrowser('chrome')
    .build();
 
driver.get('https://heroku-gitlab-staging-logan.herokuapp.com/');
 
driver.getTitle().then(function (title) {
    console.log("title is: " + title);
});
 
searchTest(driver);

function searchTest(driver) {
  driver.sleep(2000).then(function() {
    driver.getTitle().then(function(title) {
      if(title != 'wrong title') {
        console.log('Test passed');
      } else {
        console.log('Test failed');
      }
    });
  });
}

driver.quit();